import api from './api'

export const createShortUrl = (urlData) => new Promise((res, rej) => {
  api.post(`api/v1/shorten/`, urlData).then(_res => {
    if (_res.status === 200) {
      res(_res.data)
    }
    rej(_res)
  }).catch(error => {
    rej(error)
  })
})
