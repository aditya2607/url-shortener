import axios from 'axios'
import get from 'lodash/get'
import { toast } from 'react-toastify';

const REACT_APP_BACKEND_URL = 'http://54.180.202.125:3001'

const instance = axios.create({
  baseURL: REACT_APP_BACKEND_URL,
  timeout: 60000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  }
})
instance.interceptors.request.use((config) =>
  config
  , (error) => {
    toast.dismiss()
    toast.error('Oops, something went wrong')
    Promise.reject(error)
  }
)

instance.interceptors.response.use(response => {
  if (response.data && response.data.message) {
    toast.dismiss()
    toast.success(response.data.message)
  }
  return response
}, error => {
  if (error.code === 'ECONNABORTED') {
    toast.dismiss()
    toast.warn('Operation taking longer than expected...')
    return Promise.reject(error)
  }
  else {
    toast.dismiss()
    toast.error(get(error, 'response.data.message') ? error.response.data.message : error.message)
  }
  return Promise.reject(error)
})

export default instance
