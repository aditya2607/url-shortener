import React, {useState, useEffect, useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Slide';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddIcon from '@material-ui/icons/Add';
import Collapse from '@material-ui/core/Collapse';
import RemoveIcon from '@material-ui/icons/Remove';
import { PhotoCamera } from "@material-ui/icons";
import ClearIcon from '@material-ui/icons/Clear';
import { createShortUrl } from '../../utils';

const useStyles = makeStyles((theme) => ({
  title: {
    padding: theme.spacing(3),
  },
  form: {
    padding: `0 ${theme.spacing(5)}px`,
  },
  input:{
    width:'100%',
    padding: `${theme.spacing(1.25)}px 0`,
    '& .MuiInputLabel-outlined.MuiInputLabel-shrink':{
      transform:'translate(14px, 5px) scale(0.75)'
    },

  },
  btnWrapper:{
    textAlign:"center",
    position:'absolute',
    bottom:0,
    width:'100%'
  },
  button:{
    width:'100%',
    padding: theme.spacing(3),
    border:'none',
    borderRadius:0,
    background:'rgb(238, 97, 35)',
    outline:'none',
    color:'white',
    fontSize:'20px',
    fontWeight:500,
    cursor:'pointer',
  },
  settings:{
    display:'flex',
    justifyContent:'space-between'
  },
  fileInput: {
    display: "none",
  },
  faceImage: {
    color: theme.palette.primary.light,
  },
  imageNameLabel:{
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    maxWidth: '250px',
    display:'inline-block'
  },
  removeIcon:{
    color: theme.palette.primary.light,
    display:'inline-block',
    cursor:'pointer'
  },
  imageWrapper:{
    display:'flex',
    alignItems:'center',
  }
}));

export default function Home({setShowInfo, setUrlData, setLocalData, localData}) {
  const classes = useStyles();
  const [url, setUrl] =useState(localStorage.getItem('longUrl') || '');
  const [title, setTitle] =useState(localStorage.getItem('title')|| '');
  const [desc, setDesc] =useState(localStorage.getItem('desc')|| '');
  const [image, setImage] =useState(localStorage.getItem('image')|| '');
  const [ogUrl, setOgUrl] =useState(localStorage.getItem('ogUrl')|| '');
  const [loading, setLoading] =useState();
  const [showAdvanced, setShowAdvanced] =useState(localStorage.getItem('advanced')==='true');
  const [selectedFile, setSelectedFile] = useState('');
  const imagePickerRef = useRef(null);

  const handleCapture = ({ target }) => {
    setSelectedFile(target.files[0]);
  };
  const handleChange = (event) => {
    localStorage.setItem('longUrl',event.target.value.replace(/\s/g,''))
    setUrl(event.target.value.replace(/\s/g,''));
  };
  const handleTitle = (event) => {
    localStorage.setItem('title',event.target.value)
    setTitle(event.target.value);
  };
  const handleDesc = (event) => {
    localStorage.setItem('desc',event.target.value)
    setDesc(event.target.value);
  };
  const handleOgURL = (event) => {
    localStorage.setItem('ogUrl',event.target.value.replace(/\s/g,''))
    setOgUrl(event.target.value.replace(/\s/g,''));
  };
  const clearFields = () => {
    localStorage.setItem('longUrl','')
    localStorage.setItem('title','')
    localStorage.setItem('desc','')
    localStorage.setItem('image','')
    localStorage.setItem('ogUrl','')
    setUrl('');
    setTitle('');
    setDesc('');
    setImage('');
    setOgUrl('');
  }

  const getBase64 = async (file,callback) => {
    if(file){
      var x
      const reader = new FileReader();
      reader.onloadend = () =>{
        console.log(btoa(reader.result))
        callback(reader.result);
      };
      reader.readAsDataURL(file);
      return x
    }else{
      return null
    }
  }

  const handleFormSubmit = () =>{
    if(selectedFile)
      getBase64(selectedFile, submitDataToAPI)
    else
      submitDataToAPI()
  }

  const submitDataToAPI = async (baseString) => {
    setLoading(true);
    const body=`long_url=${url}&ogImageUrl=${image}&ogTitle=${title}&ogDescription=${desc}&ogUrl=${ogUrl}&Image=${btoa(baseString)}`
    console.log(body)
    createShortUrl(body)
    .then((res)=>{
      clearFields();
      setUrlData(res)
      setLoading(false)
      setShowInfo(true)
    }).catch(e => {
      console.log(e)
      setLoading(false)
    })
  }
  useEffect(()=>{
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true, 'currentWindow': true}, tabs => {
      const url = tabs[0].url
      if(url.toString().startsWith('http')){
        localStorage.setItem('longUrl',tabs[0].url)
        setUrl(tabs[0].url);
      }
    });
  },[])
  return (
    <>
      <div>
        <div className={classes.title}>
          <Typography variant={'h5'} align='center' color='primary'>
            URL Shortener
          </Typography>
        </div>
        <div  className={classes.form}>
            <TextField
              autoFocus
              type='url'
              required
              id="url"
              label="Your Long URL"
              multiline
              rowsMax={4}
              value={url}
              onChange={handleChange}
              variant="outlined"
              className={classes.input}
              placeholder="https://long-domain.com/very-long-url-with-very-long-path.html"
            />
            {
              <Collapse in={showAdvanced} className={classes.collapse}>
            <TextField
              id="og-title"
              label="Title"
              value={title}
              onChange={handleTitle}
              variant="outlined"
              className={classes.input}
              placeholder="Your URL title"
            />
            <TextField
              id="og-desc"
              label="Description"
              value={desc}
              onChange={handleDesc}
              variant="outlined"
              className={classes.input}
              placeholder="Your URL description"
            />
            <TextField
              id="og-url"
              label="OG URL"
              value={ogUrl}
              onChange={handleOgURL}
              variant="outlined"
              className={classes.input}
              placeholder="Your OG URL"
            />
            <div className={classes.imageWrapper}>

              <input
                ref={imagePickerRef}
                accept="image/*"
                className={classes.fileInput}
                type="file"
                onChange={handleCapture}
              />
                  <IconButton
                    className={classes.faceImage}
                    color="primary"
                    aria-label="upload picture"
                    onClick={()=>imagePickerRef.current.click()}
                  >
                    <PhotoCamera fontSize="large" />
                  </IconButton>
              <label className={classes.imageNameLabel}>{selectedFile ? selectedFile.name : "Select Image"}</label>
            </div>
            </Collapse>
          }

            <div className={classes.settings}>
            <Button
            color="primary"
            size='small'
            startIcon={showAdvanced ? <RemoveIcon /> : <AddIcon />}
            onClick={()=>{
              localStorage.setItem('advanced',!showAdvanced)
              setShowAdvanced(!showAdvanced)
            }}
            >Advanced Settings</Button>
            <Button color="primary" size='small' startIcon={<ClearIcon />} onClick={()=>clearFields()}>Clear</Button>
            </div>
        </div>

        <Slide direction="up" in={url?true:false} mountOnEnter unmountOnExit>
          <div className={classes.btnWrapper}>
          <button
          size="large"
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={()=>handleFormSubmit()}
          disabled={loading}
          >
          {loading ? <CircularProgress size={18} color="inherit" />:'Generate'}
        </button>
        </div>
        </Slide>
      </div>
    </>
  )
}
