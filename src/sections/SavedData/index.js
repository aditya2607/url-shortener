import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import _map from 'lodash/map';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';

const useStyles = makeStyles((theme) => ({
  appBar:{
    color:'white',
  },
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list:{
    '& .MuiTypography-root.MuiListItemText-secondary, & .MuiTypography-root.MuiListItemText-primary':{
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      maxWidth: '260px',
    }
  }
}));

export default function SavedData({setShowInfo, localData, setUrlData, updateLocalData}) {
  const classes = useStyles()
  const [loading, setLoading] =useState();

  const handleShowInfo = (data) => {
    setUrlData(data);
    setShowInfo(true);
  }
  return (
    <>
        <List className={classes.list}>
              {_map(localData,(item,index) =>
                <ListItem key={index}>
                  <ListItemText
                    primary={item.name}
                    secondary={item.long_url}
                  />
                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="info" onClick={()=>handleShowInfo(item)}>
                      <InfoOutlinedIcon/>
                    </IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={()=>updateLocalData(item,true)}>
                      <DeleteOutlineOutlinedIcon />
                    </IconButton>
                    <IconButton edge="end" aria-label="copy" onClick={()=>{navigator.clipboard.writeText(item.short_url||null);}}>
                      <FileCopyOutlinedIcon/>
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              )}
            </List>
    </>
  )
}
