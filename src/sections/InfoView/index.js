import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Slide from '@material-ui/core/Slide';
import CircularProgress from '@material-ui/core/CircularProgress';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';
import CheckIcon from '@material-ui/icons/Check';
import { toast } from 'react-toastify';

const useStyles = makeStyles((theme) => ({
  appBar:{
    color:'white',
  },
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  form: {
    padding: `${theme.spacing(2)}px ${theme.spacing(5)}px`,
  },
  input:{
    width:'100%',
    padding: `${theme.spacing(1.25)}px 0`,
    '& .MuiInputLabel-outlined.MuiInputLabel-shrink':{
      transform:'translate(14px, 5px) scale(0.75)'
    },
    '& .MuiInputAdornment-positionStart':{
      marginRight:0,
    }
  },
  btnWrapper:{
    textAlign:"center",
    position:'absolute',
    bottom:0,
    width:'100%'
  },
  button:{
    width:'100%',
    padding: theme.spacing(3),
    border:'none',
    borderRadius:0,
    background:'rgb(238, 97, 35)',
    outline:'none',
    color:'white',
    fontSize:'20px',
    fontWeight:500,
    cursor:'pointer',
  },
  detailRow:{
    display:'flex',
    justifyContent:'space-between',
    alignItems:'center',
    padding:`${theme.spacing(3)}px ${theme.spacing(5)}px`,
    borderBottom:`1px solid lightgrey`,
    fontSize:'20px',
    color:'rgb(238, 97, 35)',
    '& .domain':{
      opacity:0.7
    }
  },
  shortUrl:{

  },
  copyBtn:{
  }
}));

export default function InfoView({setShowInfo, data, allowEdit, updateLocalData}) {
  const classes = useStyles()
  const [loading, setLoading] =useState();
  const [urlData, setUrlData]=useState(data);
  const [name, setName] =useState(urlData.name||'');
  const [copy, setCopy] =useState(false);
  useEffect(()=>{
    if(copy){
      setTimeout(()=>setCopy(false),3000)
    }
  },[copy])

  const handleCopy=()=>{
    navigator.clipboard.writeText(data.short_url);
    setCopy(true)
  }

  const save = () =>{
    updateLocalData({
      name:name.trim(),
      hash:urlData.hash,
      long_url:urlData.long_url,
      ogImageUrl:urlData.ogImageUrl,
      ogTitle:urlData.ogTitle,
      ogDescription:urlData.ogDescription,
      short_url:urlData.short_url
    })
    toast.success('Saved successfully')
    setShowInfo(false)
  }
  return (
    <>
        <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={()=>setShowInfo(false)}>
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {`Short URL Info`}
          </Typography>
        </Toolbar>
        </AppBar>
        {/* <CircularProgress /> */}
        <div>
          <div className={classes.detailRow}>
            <div className={classes.shortUrl}><span className='domain'>domain.com/</span>{urlData.hash}</div>
            <div className={classes.copyBtn}>
              <Button variant="outlined" color="primary" size="small" startIcon={copy ? <CheckIcon /> : <FileCopyOutlinedIcon />} onClick={()=>handleCopy()}>
                {copy ? 'Copied' : 'Copy'}
              </Button>
            </div>
          </div>
        <div  className={classes.form}>
            <TextField
              id="name"
              label="Name"
              value={name}
              onChange={(e)=>{setName(e.target.value.trimLeft())}}
              variant="outlined"
              className={classes.input}
              placeholder="Give you URL a name to save as"
            />
        </div>

        <Slide direction="up" in={name?true:false} mountOnEnter unmountOnExit>
          <div className={classes.btnWrapper}>
          <button
          size="large"
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={()=>save()}
          disabled={loading}
          >
          {loading ? <CircularProgress size={18} color="inherit" />:'Save'}
        </button>
        </div>
        </Slide>
      </div>
    </>
  )
}
