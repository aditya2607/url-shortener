import React from 'react';
import Routes from '../../routes';

const Popup = () => {
  return (
    <div className="App">
      <Routes />
    </div>
  );
};

export default Popup;
