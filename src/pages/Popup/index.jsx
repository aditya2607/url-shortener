import React from 'react';
import { render } from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Popup from './Popup';
import './index.css';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ff9100",
      light: "#ffc246",
      dark: "c56200"
    },
    secondary: {
      main: "#ff1744",
      light: "#ff4569",
      dark: "#b2102f"
    }
  },
  typography: {
    useNextVariants: true
  }
});

render(
  <MuiThemeProvider theme={theme}>
      <Popup />
  </MuiThemeProvider>
  , window.document.querySelector('#app-container'));
