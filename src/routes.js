import React, { useState, useEffect, useContext } from 'react'
import _findIndex from 'lodash/findIndex'
import { ToastContainer, toast } from 'react-toastify'
import { makeStyles } from '@material-ui/core/styles';
import 'react-toastify/dist/ReactToastify.css'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Home from './sections/Home'
import InfoView from './sections/InfoView'
import SavedData from './sections/SavedData';

const useStyles = makeStyles((theme) => ({
  appBar:{
    color:'white'
  }
}));

export default function Routes() {
  const [value, setValue] = useState(0)
  const [showInfo, setShowInfo] = useState(false)
  const [urlData, setUrlData] = useState({})
  const [localData, setLocalData] = useState([])
  toast.configure()
  const classes = useStyles();
  useEffect(()=>{
    const data = JSON.parse(localStorage.getItem('urls'))
    if(data){
      setLocalData(data)
    }
  },[])

  const updateLocalData = (data,deleteData=false)=>{
    const index = _findIndex(localData,(item)=> item.hash === data.hash)
    if(index === -1){
      const newData = [...localData,data]
      localStorage.setItem('urls',JSON.stringify(newData))
      setLocalData(newData)
    }else{
      console.log(data)
      const newData = [...localData]
      deleteData ? newData.splice(index,1):newData.splice(index,1, data)
      localStorage.setItem('urls',JSON.stringify(newData))
      setLocalData(newData)
    }
  }

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`tabpanel-${index}`}
        {...other}
      >
        {value === index &&  children}
      </div>
    );
  }
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <>{

      showInfo ?
      <InfoView setShowInfo={setShowInfo} data={urlData} updateLocalData={updateLocalData}/> :
      <>
        <AppBar position="static" className={classes.appBar}>
          <Tabs value={value} onChange={handleChange} variant="fullWidth" textColor="inherit">
            <Tab label="Short URL"  />
            <Tab label="Saved URL" />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Home setShowInfo={setShowInfo} setUrlData={setUrlData} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <SavedData localData={localData} setUrlData={setUrlData} setShowInfo={setShowInfo} updateLocalData={updateLocalData}/>
        </TabPanel>
      </>
    }
    <ToastContainer
        position='top-center'
        autoClose={1500}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable={false}
        pauseOnHover
        limit={1}
      />
      </>
  )
}
